#!/usr/bin/python
"""
Contains methods for deciphering text which was encoded with a 
mono-aplhabetic subsitution cipher. At the moment, it only contains a simple
frequency analysis method. See the freqCrack() docstring for more info.
"""

import dictionaryUtils
import sys
from collections import Counter
import string

def createPattern(word):
	"""Create a signature of the word that is independent of substitutions
	
	This is done by substituting each occurence of a letter with a
	different one, starting from A

	EG: hello = ABCCD
		world = ABCDE
	"""
	
	pattern = list(word.strip())
	used = {}
	count = 65
	for i, letter in enumerate(pattern):
		if letter in used:
			pattern[i] = used[letter]
		else:
			used[letter] = chr(count)
			pattern[i] = chr(count)
			count += 1

	return ''.join(pattern)

def possibleWords(word, key, dict=None):
    """Find words that the pattern could possibly be.

    Returns:
        It's a generator.

    Args:
        word: the pattern to find. lowercase characters are assumed correct
        dict: the dictionary to use. If not specifed, the pattern will be
            loaded from dictionary.txt
    """
    pattern = createPattern(word)
    dictionary = dictionaryUtils.readDictionaryPatterns("dictionary.txt", [pattern]) if dict == None else dict

    # If the pattern is not in the dictionary, return an empty list
    possible = dictionary[pattern]

    revKey = {v:k for k, v in key.items()}  # Reverse the key for ease of use
    for possibility in possible:
        for i in range(len(possibility)):
            # If there is a decrypted letter that doesn't match, or a letter that should have been decrypted
            if word[i].islower() and word[i] != possibility[i] or possibility[i] in revKey and possibility[i] != word[i]:
                break
        else:
        	yield possibility

def letterFreq(text):
    """Find the frequency of letters in the cipher text.

    Returns:
        A list of lists. The inner list is 2 long. The first item is the
        character, the second is the frequency (in percents).
        The list is sorted, with the highest frequency first.
    """

    nospaces = text.replace(' ', '')
    freqs = Counter(nospaces)
    count = len(nospaces)

    #Sort the list and return it
    return sorted([[c, (t / float(count)) * 100] for c, t in freqs.items()], key=lambda i: i[1], reverse=True)

def letterCount(text):
    nospaces = text.replace(' ', '')
    freqs = Counter(nospaces)
    
    return sorted([(c, t) for c, t in freqs.items()], key = lambda i: i[1], reverse=True)

def caesarShift(text, key):
    shifted = string.ascii_uppercase[key:] + string.ascii_uppercase[:key]
    trans = string.maketrans(string.ascii_uppercase, shifted)
    return text.translate(trans)

def caesarCrack(ciphertext):
    words = ciphertext.split()
    patterns = list(set([createPattern(word) for word in words]))
    dictionary = dictionaryUtils.readDictionaryPatterns("dictionary.txt", patterns)
    for key in xrange(26):
        test = caesarShift(ciphertext, key).split()
        correctCount = 0
        for word in test:
            if word.lower() in dictionary[createPattern(word)]:
                correctCount += 1
        if correctCount/ float(len(words)) >= 0.5:
            return key
    return -1

def freqCrack(ciphertext, key={}):
    """Attempt to automatically decipher the text.

    It assumes that the most common letter in the text is e, so it works
    best on larger messages. It then goes through every word, looking for
    ones with only one possibility. If it finds one, it will assume that it
    is correct and adjust the key accordingly. It will continue like this
    until no more changes are made.
    """

    #Assume that the most common character in the cipher text is e
    freqList = letterFreq(ciphertext)
    key[freqList[0][0].lower()] = 'e'
    print "Assuming", freqList[0][0], "is e based on frequency"

    #Get a list of words without repeats
    words = list(set(ciphertext.split()))

    #Find all of the patterns in the list of words
    patterns = list(set([createPattern(word) for word in words]))

    #Read all of the words that match the patterns from the dictionary
    dictionary = dictionaryUtils.readDictionaryPatterns("dictionary.txt", patterns)

    #Loop until a loop is done until no changes are made to the key
    changed = True
    while changed:
        changed = False
        for word in words:  # Loop through all of the words
            # Find all of the possible decryptions
            possibilities = list(possibleWords(decode(word, key), key, dictionary))

            #If there is only one, assume it is correct
            if len(possibilities) == 1:
                possibility = possibilities[0]

                #Set all of the letters from the possibility in the key
                for i in range(len(possibility)):
                    if not word[i].lower() in key:
                        changed = True
                        key[word[i].lower()] = possibility[i]

                        #Print an update
                        print "Setting", word[i].lower(), "=", possibility[i], "Based on", word, "=", possibility

    return key

def decode(text, key):
        """Decode a passage using the key

        Args:
            The word or passage to decode. If not specified, use the ciphertext
        """

        letters = list(text.upper())
        for i in range(len(letters)):
            if letters[i].lower() in key:
                letters[i] = key[letters[i].lower()]
        return ''.join(letters)

def processText(text):
    """Remove all non-alphanumeric characters except spaces."""
    return ' '.join([''.join(e for e in word if e.isalnum()) for word in text.upper().split()])

def main():
	ciphertext = processText(open("TheRavenRot13.txt").read())
	print "\nCipher:", ciphertext, "\n"
	
	key = freqCrack(ciphertext)
	print "\nDecryption:\n", decode(ciphertext, key)

if __name__ == "__main__": 
    main()