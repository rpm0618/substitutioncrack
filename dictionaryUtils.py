#!/usr/bin/python
"""Contains functions to deal with pattern dictionaries

The only methods that should be called from code are readDictionary and
readPatternDictionary. The rest is for command line usage.

Dictionary Format:
	Each pattern is prefaced by a colon(:). The words that follow
	are part of that pattern, until another pattern is specified.
"""
import sys
import subcrack

from collections import defaultdict

def readDictionary(dictionaryFilename):
	"""Read in a pattern dictionary from a file"""
	
	dictionary = defaultdict(list)
	curPattern = "" #The pattern that is being read in

	with open(dictionaryFilename) as dictionaryFile:
		for word in dictionaryFile:
			word = word.strip()
			if word[0] == ":": #If the line begins with a :, it is a new pattern
				curPattern = word[1:]
				dictionary[curPattern] = []
			else: #Otherwise, its a word from the previous pattern
				dictionary[curPattern].append(word)

	return dictionary

def readDictionaryPatterns(dictionaryFilename, patterns):
	"""Read the specified patterns in from a pattern dictionary"""

	dictionary = defaultdict(list)
	curPattern = ""

	with open(dictionaryFilename) as dictionaryFile:
		reading = False
		for word in dictionaryFile:
			word = word.strip()
			if word[0] == ":":
				if word[1:] in patterns:
					curPattern = word[1:]
					reading = True
					dictionary[curPattern] = []
				else:
					reading = False
			elif reading:
				dictionary[curPattern].append(word)

	return dictionary

def listDictionary(dictionaryFilename, pattern):
	"""List all of the words with the specified pattern"""
	
	print readDictionaryPatterns(dictionaryFilename, pattern)[pattern]

def addWords(dictionaryFilename, words):
	"""Add the words given to the dictionary given"""
	
	print "Reading Dictionary...",
	dictionary = readDictionary(dictionaryFilename)
	print "Done\n"
	
	for word in words: 
		pattern = subcrack.createPattern(word)
		if len(words) <= 20: print "Adding", word, "with pattern", pattern
		if pattern in dictionary and word not in dictionary[pattern]:
			dictionary[pattern].append(word)
		elif pattern not in dictionary:
			dictionary[pattern] = [word]
	
	print "Added", len(words), "words"
	
	writeDictionary(dictionary, dictionaryFilename)

def addCorpus(dictionaryFilename, corpusFilename):
	"""Add the words in the file to the dictionary
	
	The file is expected to contain a selection of natural text, punctuation
	and all. It will word with a wordlist, but will be slower, and is 
	therefore not recommended
	"""
	
	corpusText = open(corpusFilename).read()
	words = list(set([''.join(e for e in word if e.isalnum()) for word in corpusText.replace('-', ' ').lower().split()]))
	addWords(dictionaryFilename, words)

def addList(dictionaryFilename, wordlistFilename):
	"""Add the words in the file to the dictionary"""
	
	wordList = open(wordlistFilename).read().split()
	addWords(dictionaryFilename, wordList)

def mergeDictionary(dictionary1Filename, dictionary2Filename, outputFilename):
	"""Merge two dictionaries together, without duplicates"""
	
	print "Reading Dictionary...",
	dictionary1 = readDictionary(dictionary1Filename)
	dictionary2 = readDictionary(dictionary2Filename)
	print "Done\n"
	
	for pattern in dictionary2:
		if pattern in dictionary1:
			dictionary1[pattern] = list(set(dictionary1[pattern].extend(dictionary2[pattern])))
		else:
			dictionary1[pattern] = dictionary2[pattern]
			
	writeDictionary(outputFilename)
	
def dictionaryStats(dictionaryFilename):
	"""Calculate and print statistics about the dictionary file given"""
	
	print "Reading Dictionary...",
	dictionary = readDictionary(dictionaryFilename)
	print "Done\n"
	print len(dictionary), "Patterns"
	
	totalWordCount = 0
	averagePatternCount = 0
	largestPattern = ("", 0)
	
	for pattern in dictionary:
		patternWordCount = len(dictionary[pattern])
		totalWordCount += patternWordCount
		
		#Compute a running average of length of each pattern
		if averagePatternCount == 0:
			averagePatternCount = patternWordCount
		else:
			averagePatternCount += patternWordCount
			averagePatternCount /= 2.0
		
		#Find the largest pattern
		if patternWordCount > largestPattern[1]:
			largestPattern = (pattern, patternWordCount)
	
	#Print the statistics
	print totalWordCount, "Words\n"
	
	print "Average Pattern Length:", averagePatternCount, "\n"
	
	print "Largest Pattern:", largestPattern[0]
	print "\tLength:", largestPattern[1]

def writeDictionary(dictionary, dictionaryFilename):
	"""Write a pattern dictionary to a file"""
	
	print "Writing list to file...",
	
	wordcount = 0
	dictionaryFile = open(dictionaryFilename, "w")
	for pattern in dictionary:
		#Write the pattern (preceded by a colon)
		dictionaryFile.write(":" + pattern + "\n")
		words = dictionary[pattern]
		
		#Write all of the words to the file
		dictionaryFile.write('\n'.join(words) + "\n")
		wordcount += len(words)
	
	dictionaryFile.close()
	
	print "Done. Wrote", wordcount, "words"
	
def createDictionary(wordListFilename, dictionaryFilename):
	"""Create a pattern dictionary from the words in the word list
	
	Arguments:
		wordListFilename
			A list of all the words that should go into the dictionary.
			Each word should be on one line
			
		dictionaryFilename
			The file to output the dictionary to
	"""
	
	wordList = open(wordListFilename)
	dictionary = {}
	
	print "Creating list of patterns:"
	
	patterncount = 0
	for word in wordList:
		word = word.strip()
		pattern = subcrack.createPattern(word)
		
		#If the pattern is already in the dictionary, then add the word
		#to the list. Otherwise, add the pattern to the dictionary
		if pattern in dictionary:
			dictionary[pattern].append(word)
		else:
			dictionary[pattern] = [word]
			patterncount += 1
			
			#Print a status message every 5000 patterns
			if patterncount % 5000 == 0: print patterncount, "patterns"
			
	wordList.close()
	print "Found", patterncount, "patterns\n"
	
	writeDictionary(dictionaary, dictionaryFilename)

def printUsage():
	print "Usage:"
	print "\t python dictionaryUtils.py create <wordlist> <output>"
	print "\t python dictionaryUtils.py stats <dict>"
	print "\t python dictionaryUtils.py list <dict> <pattern>"
	print "\t python dictionaryUtils.py add <dict> [words]"
	print "\t python dictionaryUtils.py add-list <dict> <wordlsit>"
	print "\t python dictionaryUtils.py add-corpus <dict> <corpus>"
	print "\t python dictionaryUtils.py merge <dict1> <dict2> <output>"

def main():
	if len(sys.argv) < 2:
		printUsage()
		return
	
	function = sys.argv[1]
	
	if function == "create" and len(sys.argv) == 4:
		createDictionary(sys.argv[2], sys.argv[3])
	elif function == "stats" and len(sys.argv) == 3:
		dictionaryStats(sys.argv[2])
	elif function == "list" and len(sys.argv) == 4:
		listDictionary(sys.argv[2], sys.argv[3])
	elif function == "add-corpus" and len(sys.argv) == 4:
		addCorpus(sys.arv[2], sys.argv[3])
	elif function == "add-list" and len(sys.argv) == 4:
		addList(sys.argv[2], sys.argv[3])
	elif function == "add" and len(sys.argv) >= 4:
		addWords(sys.argv[2], sys.argv[3:])
	elif function == "merge" and len(sys.argv) == 5:
		mergeDictionary(sys.argv[2], sys.argv[3], sys.argv[4])
	else:
		printUsage()
		return
	
if __name__ == "__main__": main()