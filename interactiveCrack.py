#!/usr/bin/python
"""
A small interactive console to help crack monoalphabetic substitution ciphers.

Makes decoding easier by placing known letters into the text, finding possible
words based on the word pattern and known key, and providing a letter frequency
counter. There is also an expirimental automatic cracking functionality, which
usually performs very well.

Assumes that there is a dictionary file named dictionary.txt in the working
directory.
"""

import subcrack
import dictionaryUtils

import cmd
import os
from collections import Counter


class CrackCmd(cmd.Cmd, object):
    """The actual console object, based on the cmd module"""

    ciphertext = ""
    working = ""
    key = {}

    def do_shell(self, s):
        """Call a system command"""
        os.system(s)

    def do_exit(self, s):
        """Exit the session"""
        return True

    def do_cd(self, s):
        """Change the current working directory"""
        os.chdir(s)

    def do_pwd(self, s):
        """Print the current working directory"""
        print os.getcwd()

    def do_ls(self, s):
        """List the contents of a directory"""
        
        if s == '':  # If no directory was given, use the current one.
            print "Listing contents of:", os.getcwd(), "\n"
            print '\n'.join(os.listdir(os.getcwd()))
        elif os.path.isdir(s):
            print "Listing contents of:", s, "\n"
            print '\n'.join(os.listdir(s))
        else:
            print "*** Not a valid directory:", s

    def do_load(self, s):
        """Load ciphertext into memory"""
        text = open(s).read()

        #Get rid of punctuation
        self.ciphertext = subcrack.processText(text)
        self.working = self.ciphertext[:]

    def complete_load(self, text, line, beginidx, endidx):
        """The function used by the cmd module to auto-complete the load command
        with filenames"""
        return self.completeFile(text)

    def completeFile(self, text):
        """Return a list of files that start with the given text"""
        return [i for i in os.listdir(os.getcwd()) if i.startswith(text) and os.path.isfile(i)]

    _availableBuffers = ["ciphertext", "working", "key"]

    def do_print(self, s):
        """Print the contents of the specified buffer to the screen"""

        if s == "ciphertext":
            print self.ciphertext
        elif s == "working":
            print self.working
        elif s == "key":
            print '\n'.join(k + ": " + v for k, v in self.key.items())
        else:
            print "*** Unknown buffer:", s

    def complete_print(self, text, line, beginidx, endidx):
        """The function used by the cmd module to auto-complete the print
        command with buffer names"""
        return [i for i in self._availableBuffers if i.startswith(text)]

    def do_freq(self, s):
        """Calculate the frequncies of every letter"""
        freqList = subcrack.letterFreq(self.ciphertext)
        print '\n'.join(f[0] + ": " + "%.2f" % f[1] for f in freqList)

    def do_count(self, s):
        """Output the number of each character in the ciphertext"""
        countList = subcrack.letterCount(self.ciphertext)
        print '\n'.join(c[0] + ": " + str(c[1]) for c in countList)

    def do_set(self, s):
        """Set a letter in the key"""
        args = s.split()
        self.key[args[0].lower()] = args[1].lower()
        self.working = subcrack.decode(self.ciphertext, self.key)

    def do_possible(self, s):
        """Find possible words, based on the key and pattern.

        Lower case letters are assumed correct.
        """
        print '\n'.join(subcrack.possibleWords(s, self.key))

    def do_autocrack(self, s):
        """Attempt to automatically decipher the text.

        It assumes that the most common letter in the text is e, so it works
        best on larger messages. It then goes through every word, looking for
        ones with only one possibility. If it finds one, it will assume that it
        is correct and adjust the key accordingly. It will continue like this
        until no more changes are made.
        """

        key = subcrack.caesarCrack(self.ciphertext)
        if key != -1:
            self.working = subcrack.caesarShift(self.ciphertext, key)
            print "Caesar Shift, key:", key
            print "\nDecryption:\n", self.working
            return
        else:
            print "Caesar Shift failed, attempting frequency analysis\n"

        key = subcrack.freqCrack(self.ciphertext, self.key)
        self.working = subcrack.decode(self.ciphertext, key)
        print "\nDecryption:\n", self.working

def main():
    crack = CrackCmd()
    crack.cmdloop()

if __name__ == "__main__":
    main()
